#!/usr/bin/env bash
#
#
#
BUILD_IMAGE=gapps-build:latest

docker build -t $BUILD_IMAGE docker/

docker run --rm -v ${PWD}/build/:/run/build $BUILD_IMAGE
