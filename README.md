# anbox android arm+gapps

build and install anbox and latest possible android with ARM and gapps

This is based on the "install-playstore.sh" script from https://github.com/geeks-r-us/anbox-playstore-installer.

I created a docker-based build process to avoid running a install script as root.

## Installation steps

### 1 install anbox
* use apt on debian and children
* use snap anywhere else


### 2 build / download dependencies
`./build-dependencies.sh`

This will download a working android and gapps image.

### 3 build the modified android image
`./build-gapps.sh`

This will build gapps and arm mapping. The resulting image is: **./build/android.img**

### 4 copy the built image to your anbox execution environment

**debian**

`sudo cp ./build/android.img /var/lib/anbox/android.img`

**snap**

`sudo cp ./build/android.img /var/snap/anbox/common/android.img`

### Advice: restart anbox container manager and delete all files

**debian**

`sudo systemctl stop anbox-container-manager.service`

`sudo rm -rf /var/lib/anbox/*`

`sudo systemctl start anbox-container-manager.service`

**snap**

`sudo snap stop anbox.container-manager`

`sudo rm -rf /var/snap/anbox/common/*`

`sudo snap start anbox.container-manager`