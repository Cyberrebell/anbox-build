#!/usr/bin/env bash
#
#
#
set -e

OVERLAY_DIR=./anbox/common/rootfs-overlay

if [[ -d ./anbox ]]
then
    echo "removing ./build/anbox/"
    rm -rf ./anbox
fi

echo "unquash android"
if [[ -d ./squashfs-root ]]
then
    rm -rf ./squashfs-root
fi
unsquashfs android_amd64.img


echo "extract opengapps"
if [[ -d ./opengapps ]]
then
    rm -rf ./opengapps
fi
unzip -d ./opengapps opengapps.zip

pushd ./opengapps/Core/
for filename in *.tar.lz
do
    tar --lzip -xvf ./$filename
done
popd


echo "copy gapps files to app dir"
app_dir="${OVERLAY_DIR}/system/priv-app"
if [[ ! -d ${app_dir} ]]
then
    mkdir -p ${app_dir}
fi

cp -r ./$(find opengapps -type d -name "PrebuiltGmsCore") ${app_dir}
cp -r ./$(find opengapps -type d -name "GoogleLoginService") ${app_dir}
cp -r ./$(find opengapps -type d -name "Phonesky") ${app_dir}
cp -r ./$(find opengapps -type d -name "GoogleServicesFramework") ${app_dir}

pushd ${app_dir}
chown -R 100000:100000 Phonesky GoogleLoginService GoogleServicesFramework PrebuiltGmsCore
popd


echo "unquash houdini"
houdini_dir=./houdini
if [[ ! -d ${houdini_dir} ]]
then
    mkdir ${houdini_dir}
fi
unsquashfs -f -d ${houdini_dir} ./houdini.sfs


echo "prepare system/bin/"
bin_dir="${OVERLAY_DIR}/system/bin"
if [[ ! -d ${bin_dir} ]]
then
    mkdir -p ${bin_dir}
fi
cp -r "${houdini_dir}/houdini" "${bin_dir}/"
cp -r "${houdini_dir}/xstdata" "${bin_dir}/"
chown -R 100000:100000 "${bin_dir}/houdini" "${bin_dir}/xstdata"


echo "prepare system/lib/"
lib_dir="${OVERLAY_DIR}/system/lib"
if [[ ! -d ${lib_dir} ]]
then
    mkdir -p ${lib_dir}
fi
cp ./libhoudini.so "${lib_dir}/"
chown 100000:100000 "${lib_dir}/libhoudini.so"

mkdir "${lib_dir}/arm"
cp -r "${houdini_dir}/linker" "${lib_dir}/arm"
cp -r ${houdini_dir}/*.so "${lib_dir}/arm"
cp -r "${houdini_dir}/nb" "${lib_dir}/arm"
chown -R 100000:100000 "${lib_dir}/arm"


echo "prepare system/etc/binfmt_misc/"
binfmt_misc_dir="${OVERLAY_DIR}/system/etc/binfmt_misc"
if [[ ! -d ${binfmt_misc_dir} ]]
then
    mkdir -p ${binfmt_misc_dir}
fi
echo ":arm_dyn:M::\x7f\x45\x4c\x46\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x28::/system/bin/houdini:" > "${binfmt_misc_dir}/arm_dyn"
echo ":arm_exe:M::\x7f\x45\x4c\x46\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28::/system/bin/houdini:" > "${binfmt_misc_dir}/arm_exe"
chown -R 100000:100000 ${binfmt_misc_dir}


echo "add android features"
features=$(cat <<-END
  <feature name="android.hardware.touchscreen" />\n
  <feature name="android.hardware.audio.output" />\n
  <feature name="android.hardware.camera" />\n
  <feature name="android.hardware.camera.any" />\n
  <feature name="android.hardware.location" />\n
  <feature name="android.hardware.location.gps" />\n
  <feature name="android.hardware.location.network" />\n
  <feature name="android.hardware.microphone" />\n
  <feature name="android.hardware.screen.portrait" />\n
  <feature name="android.hardware.screen.landscape" />\n
  <feature name="android.hardware.wifi" />\n
  <feature name="android.hardware.bluetooth" />"
END
)
features=$(echo ${features} | sed 's/\//\\\//g')
features=$(echo ${features} | sed 's/\"/\\\"/g')

permissions_dir="${OVERLAY_DIR}/system/etc/permissions"
if [[ ! -d ${permissions_dir} ]]
then
    mkdir -p ${permissions_dir}
    cp ./squashfs-root/system/etc/permissions/anbox.xml "${permissions_dir}/"
fi
sed -i "/<\/permissions>/ s/.*/${features}\n&/" "${permissions_dir}/anbox.xml"


echo "make wifi and bt available"
sed -i "/<unavailable-feature name=\"android.hardware.wifi\" \/>/d" "${permissions_dir}/anbox.xml"
sed -i "/<unavailable-feature name=\"android.hardware.bluetooth\" \/>/d" "${permissions_dir}/anbox.xml"


echo "copy squashfs-root/system/build.prop"
if [[ ! -x "${OVERLAY_DIR}/system/build.prop" ]]
then
    cp ./squashfs-root/system/build.prop "${OVERLAY_DIR}/system/build.prop"
fi


echo "set processors"
ARM_TYPE=",armeabi-v7a,armeabi"
sed -i "/^ro.product.cpu.abilist=x86_64,x86/ s/$/${ARM_TYPE}/" "${OVERLAY_DIR}/system/build.prop"
sed -i "/^ro.product.cpu.abilist32=x86/ s/$/${ARM_TYPE}/" "${OVERLAY_DIR}/system/build.prop"

echo "persist.sys.nativebridge=1" >> "${OVERLAY_DIR}/system/build.prop"

echo "enable opengles"
echo "ro.opengles.version=131072" >> "${OVERLAY_DIR}/system/build.prop"


echo "creating combined android image"
cp -r ${OVERLAY_DIR}/* ./squashfs-root/
mksquashfs ./squashfs-root/* android.img
