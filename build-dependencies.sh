#!/usr/bin/env bash
#
# download / build dependencies
#

# https://build.anbox.io/android-images/
if [[ ! -f ./build/android_amd64.img ]]
then
    wget -O ./build/android_amd64.img https://build.anbox.io/android-images/2018/07/19/android_amd64.img
fi

# https://opengapps.org/
# https://github.com/opengapps/x86_64
if [[ ! -f ./build/opengapps.zip ]]
then
    wget -O ./build/opengapps.zip https://github.com/opengapps/x86_64/releases/download/20180903/open_gapps-x86_64-7.1-mini-20180903.zip
fi

# https://github.com/Rprop/libhoudini
if [[ ! -f ./build/houdini.sfs ]]
then
    wget -O ./build/houdini.sfs http://dl.android-x86.org/houdini/7_y/houdini.sfs
fi
if [[ ! -f ./build/libhoudini.so ]]
then
    wget -O ./build/libhoudini.so https://github.com/Rprop/libhoudini/raw/master/4.0.8.45720/system/lib/libhoudini.so
fi
